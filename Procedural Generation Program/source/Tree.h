#pragma once
#include <glm\mat4x4.hpp>

class FBXModel;
class Camera;
class DirectionalLight;

class Tree
{
private:
 	FBXModel* m_pModel;
	glm::mat4 m_worldTransform;

public:
	Tree();

	void Draw(const Camera& a_camera, const DirectionalLight& a_dirLight);

	void SetPosition(const glm::vec3& a_position);

	inline void SetModel(FBXModel* a_pModel) { m_pModel = a_pModel; }
};