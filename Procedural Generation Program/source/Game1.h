#pragma once
#include <Game.h>
#include <vector>
#include <Camera\FlyCamera.h>
#include <Terrain.h>
#include <Tree.h>
#include <Render\Lighting\DirectionalLight.h>
#include <ParticleSystem.h>

class TweakBar;

class Game1 : public Game
{
private:
	Terrain m_terrain;

	FlyCamera m_camera;

	TweakBar* m_pTweakBar;

	bool m_lastBarPress;	// Last state of button that toggles tweak bar display
	bool m_showTweakBar;
	bool m_wireFrame;
	bool m_drawGizmos;
	bool m_drawTerrainNorm;

	int m_frameRate, m_frameCounter;
	float m_frameElaspedTime;

	DirectionalLight m_dirLight;

	ParticleSystem m_particleSystem;
	
	// Tree vairiables
	FBXModel* m_pTreeModel;
	Tree* m_pTree;
	glm::vec3* m_pTreePos;
	unsigned int m_treeCount, m_maxTrees;
	void DrawTrees();

public:
	Game1();
	virtual ~Game1();

	void Initialise();
	void Update(const float &a_deltaTime);
	void Draw(const float &a_deltaTime);
	void Shutdown();

	void Generate();
};
