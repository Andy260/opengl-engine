#include "TweakBar.h"
#include <iostream>
#include <GameWindow.h>
#include <Game1.h>
#include <Terrain.h>
#include <Render\Lighting\DirectionalLight.h>
#include <glm\ext.hpp>


Terrain* TweakBar::m_pTerrain(nullptr);
DirectionalLight* TweakBar::m_pDirLight(nullptr);
Game1* TweakBar::m_pGame(nullptr);
float TweakBar::m_frequency(0.0f);
float TweakBar::m_amplitude(0.0f);
float TweakBar::m_persistence(0.0f);
float TweakBar::m_octaves(0.0f);
float TweakBar::m_lightAmbient(1.0f);
float TweakBar::m_lightDifInten(1.0f);
float TweakBar::m_lightSpec(1.0f);
glm::vec3 TweakBar::m_lightColour(1.0f, 1.0f, 1.0f);
glm::vec3 TweakBar::m_lightDir(0.0f, -1.0f, 0.0f);
unsigned int TweakBar::m_terrainSeed(0u);

TweakBar::TweakBar(const int& a_winWidth, const int& a_winHeight,
				   int* a_pFPS, Terrain* a_pTerrain,
				   bool* a_pWireFrame, bool* a_terrainNormals,
				   DirectionalLight* a_pDirLight, unsigned int* a_treeCount, 
				   unsigned int* a_pMaxTrees, Game1* a_pGame)
{
	// Assign terrain variables
	m_pTerrain = a_pTerrain;
	m_terrainSeed = m_pTerrain->GetSeed();
	m_frequency = m_pTerrain->GetFrequency();
	m_amplitude = m_pTerrain->GetAmplitude();
	m_persistence = m_pTerrain->GetPersistence();
	m_octaves = m_pTerrain->GetOctaves();

	// Intiailise lighting variables
	m_pDirLight = a_pDirLight;
	m_lightColour = m_pDirLight->GetColour();
	m_lightAmbient = m_pDirLight->GetAmbientIntensity();
	m_lightDir = m_pDirLight->GetDirection();
	m_lightDifInten = m_pDirLight->GetDiffuseIntensity();

	// Assign Tree generation variables
	m_pGame		= a_pGame;
	m_pMaxTrees = a_pMaxTrees;

	// Initialise AntTweakBar and create bar
	TwInit(TW_OPENGL_CORE, nullptr);
	m_tweakBar = TwNewBar("Tweak Bar");

	// Set initial window size
	TwWindowSize(a_winWidth, a_winHeight);

	// Add rendering tweakables
	AddRenderingOptions(a_pFPS, a_pWireFrame);

	// Add Terrain generation variables
	AddTerrainOptions(a_terrainNormals, a_treeCount, a_pMaxTrees);

	// Add Lighting options
	AddLightingOptions();
}

TweakBar::~TweakBar()
{
	TwDeleteAllBars();
	TwTerminate();
}

void TweakBar::GenerateTerrain()
{
	m_pTerrain->SetSeed(m_terrainSeed);
	m_pTerrain->SetFrequency(m_frequency);
	m_pTerrain->SetAmplitude(m_amplitude);
	m_pTerrain->SetPersistence(m_persistence);
	m_pTerrain->SetOctaves(m_octaves);

	m_pTerrain->Generate();
}

void TweakBar::GenerateTrees()
{
	m_pGame->Generate();
}

void TweakBar::CheckStatus(const int& a_status)
{
	// Check status
	if (a_status == 0)
		std::cout << "Tweak Bar Error: " << TwGetLastError() << std::endl;
}

void TweakBar::AddRenderingOptions(int* a_pFPS, bool* a_pWireFrame)
{
	int status = TwAddVarRO(m_tweakBar, "FPS: ", TW_TYPE_INT32, a_pFPS,
		"label='FPS: ' group='Rendering'");
	CheckStatus(status);

	// Add wireframe mode
	status = TwAddVarRW(m_tweakBar, "Wireframe", TW_TYPE_BOOLCPP,
		a_pWireFrame,
		"label='Wireframe Mode' help='Toggle rendering of wireframe mode' group='Rendering'");
	CheckStatus(status);
}

void TweakBar::AddTerrainOptions(bool* a_terrainNormals, unsigned int* a_pTreeCount, 
								 unsigned int* a_pMaxTrees)
{
	// Seed
	int status = TwAddVarRW(m_tweakBar, "Terrain Seed", TW_TYPE_UINT32, 
		&m_terrainSeed, "label='Seed' group='Terrain'");
	CheckStatus(status);

	// Frequency
	status = TwAddVarRW(m_tweakBar, "Frequency", TW_TYPE_FLOAT, &m_frequency,
		"label='Frequency' group='Terrain'");
	CheckStatus(status);

	// Amplitude
	status = TwAddVarRW(m_tweakBar, "Amplitude", TW_TYPE_FLOAT, &m_amplitude,
		"label='Amplitude' group='Terrain'");
	CheckStatus(status);

	// Persistence
	status = TwAddVarRW(m_tweakBar, "Persistence", TW_TYPE_FLOAT, &m_persistence,
		"label='Persistence' group='Terrain' step=0.01");
	CheckStatus(status);

	// Octaves
	status = TwAddVarRW(m_tweakBar, "Octaves", TW_TYPE_FLOAT, &m_octaves,
		"label='Octaves' group='Terrain'");
	CheckStatus(status);

	// Normals rendering
	status = TwAddVarRW(m_tweakBar, "Terrain Normals", TW_TYPE_BOOLCPP,
		a_terrainNormals, "label='Draw Normals' group='Terrain'");

	// Add tree options
	std::string def = "label='Amount of Trees' group='Terrain' max='" + std::to_string((*m_pMaxTrees)) + "'";
	status = TwAddVarRW(m_tweakBar, "Tree count", TW_TYPE_UINT32, a_pTreeCount,
		def.c_str());
	CheckStatus(status);

	// Generate Terrain button
	status = TwAddButton(m_tweakBar, "Generate Terrain",
		[](void* a_clientData) -> void
	{
		TweakBar::GenerateTerrain();
		TweakBar::GenerateTrees();
	},
		NULL, "label='Generate Terrain' group='Terrain'");
	CheckStatus(status);
}

void TweakBar::AddLightingOptions()
{
	// Lighting Direction
	int status = TwAddVarRW(m_tweakBar, "DirLight Direction", TW_TYPE_DIR3F, 
		&m_lightDir, "label='Direction' group='Lighting'");
	CheckStatus(status);

	// Diffuse Intensity
	status = TwAddVarRW(m_tweakBar, "DirLight Diffuse", TW_TYPE_FLOAT,
		&m_lightDifInten, "label='Diffuse Intensity' group='Lighting' step=0.01 max=1.0 min=0.0");
	CheckStatus(status);

	// Directional Light colour
	status = TwAddVarRW(m_tweakBar, "DirLight Colour", TW_TYPE_COLOR3F, 
		&m_lightColour[0], "label='Colour' group='Lighting'");
	CheckStatus(status);

	// Ambient light Intensity
	status = TwAddVarRW(m_tweakBar, "DirLight Ambient", TW_TYPE_FLOAT, 
		&m_lightAmbient, "label='Ambient Intensity' group='Lighting' step=0.01 max=1.0 min=0.0");

	// Specular Highlights intensity
	status = TwAddVarRW(m_tweakBar, "DirLight Specular", TW_TYPE_FLOAT,
		&m_lightSpec, "label='Specular Intensity' group='Lighting'");
}

void TweakBar::Draw()
{
	// Update lighting properties
	m_lightDir = glm::normalize(m_lightDir);
	m_pDirLight->SetColour(m_lightColour);
	m_pDirLight->SetAmbientIntensity(m_lightAmbient);
	m_pDirLight->SetDiffuseIntensity(m_lightDifInten);
	m_pDirLight->SetDirection(m_lightDir);

	// Draw tweak bar
	TwDraw();
}