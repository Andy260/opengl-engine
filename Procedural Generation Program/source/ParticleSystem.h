#pragma once
#include <Particle\ParticleEmitter.h>
#include <vector>
#include <glm\vec3.hpp>

class ParticleSystem : public ParticleEmitter
{
private:
	std::vector<glm::vec3> m_positions;

public:
	ParticleSystem();
	virtual ~ParticleSystem();
};