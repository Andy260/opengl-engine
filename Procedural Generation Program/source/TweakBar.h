#pragma once
#include <AntTweakBar.h>
#include <glm\fwd.hpp>

class Terrain;
class DirectionalLight;
class Game1;

class TweakBar
{
private:
	TwBar* m_tweakBar;

	// Terrain control
	static unsigned int m_terrainSeed;
	static Terrain* m_pTerrain;
	static float m_frequency, m_amplitude, m_persistence, m_octaves;
	static void GenerateTerrain();

	// Helper functions
	void CheckStatus(const int& a_status);

	// Buttons/options generation
	void AddRenderingOptions(int* a_pFPS, bool* a_pWireFrame);
	void AddTerrainOptions(bool* a_terrainNormalsm, unsigned int* a_pTreeCount, 
						   unsigned int* a_pMaxTrees);
	void AddLightingOptions();

	// Lighting Control
	static DirectionalLight* m_pDirLight;
	static glm::vec3 m_lightDir;
	static glm::vec3 m_lightColour;
	static float m_lightAmbient;
	static float m_lightDifInten;	// Diffuse intensity
	static float m_lightSpec;

	// Tree generation
	unsigned int* m_pMaxTrees;
	static Game1* m_pGame;
	static void GenerateTrees();

public:
	TweakBar(const int& a_winWidth, const int& a_winHeight,
			 int* a_pFPS, Terrain* a_pTerrain,
			 bool* a_pWireFrame, bool* a_terrainNormals, 
			 DirectionalLight* a_pDirLight, unsigned int* a_treeCount, 
			 unsigned int* a_pMaxTrees, Game1* m_pGame);
	~TweakBar();

	void Draw();
};