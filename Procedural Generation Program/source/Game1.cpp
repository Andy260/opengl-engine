#include "Game1.h"
#include <gl_core_4_4.h>
#include <glm\ext.hpp>
#include <Input\Keyboard.h>
#include <Gizmos.h>
#include <GameWindow.h>
#include <TweakBar.h>
#include <Content\Model\FBXModel.h>


Game1::Game1()
	: Game(1280, 720, "Procedural Generation - Andrew Barbour"),
	m_camera(m_pWindow),
	m_terrain(100),
	m_lastBarPress(false),
	m_showTweakBar(true),
	m_frameRate(0),
	m_frameCounter(0),
	m_frameElaspedTime(0.0f),
	m_drawGizmos(true),
	m_wireFrame(false),
	m_drawTerrainNorm(false),
	m_dirLight(glm::vec3(1.0f, 1.0f, 1.0f), 0.1f, glm::vec3(0, -1, 0), 1.0f, 1.0f),
	m_treeCount(50),
	m_maxTrees(50)
{
	
}

void Game1::Initialise()
{
	// Setup Rendering
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	// Enable alpha blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Seed random for tree position picking
	srand((unsigned int)time(NULL));

	Gizmos::create();

	// Create tweak bar
	int winWidth, winHeight;
	m_pWindow->GetSize(&winWidth, &winHeight);

	m_pTweakBar = new TweakBar(winWidth, winHeight, &m_frameRate, &m_terrain, &m_wireFrame, 
							   &m_drawTerrainNorm, &m_dirLight, &m_treeCount, &m_maxTrees, 
							   this);

	// Setup camera
	m_camera.LookAt(glm::vec3(0, 10, 10), glm::vec3(0, 0, 0), 
		glm::vec3(0, 1, 0));
	m_camera.SetProjection(glm::pi<float>() * 0.25f, 
		(float)winWidth / (float)winHeight, 0.1f, 1000.0f);
	m_camera.SetSpeed(10.0f);

	// Intiailise Terrain
	m_terrain.Initialise();
	m_terrain.SetSeed((float)(rand() % 1000));
	m_terrain.Generate();

	// Initialise Trees
	m_pTree			= new Tree[m_maxTrees];
	m_pTreeModel	= new FBXModel[2];
	m_pTreeModel[0].Load("./models/ALan Tree/AlanTree.fbx");
	m_pTreeModel[1].Load("./models/Tree/treeplan1.fbx");
	Generate();

	m_particleSystem.Initialise(1000, 500,
		0.1f, 1.0f,
		1, 5,
		1, 0.1f,
		glm::vec4(1, 0, 0, 1), glm::vec4(1, 1, 0, 1), 
		"./textures/star.png");
}

void Game1::Update(const float &a_deltaTime)
{
	m_camera.Update(a_deltaTime);

	if (Keyboard::IsKeyDown(Keys::KEY_SPACE) && !m_lastBarPress)
	{
		m_showTweakBar = !m_showTweakBar;
	}

	m_lastBarPress = Keyboard::IsKeyDown(Keys::KEY_SPACE);

	m_frameElaspedTime += a_deltaTime;

	if (m_frameElaspedTime > 1.0f)
	{
		m_frameElaspedTime = 0.0f;
		m_frameRate = m_frameCounter;
		m_frameCounter = 0;
	}

	m_particleSystem.Update(a_deltaTime, m_camera.GetTransform());
}

void Game1::Generate()
{
	unsigned int terrainSize = m_terrain.GetSize() * m_terrain.GetSize();

	srand((unsigned int)time(NULL));

	for (unsigned int i = 0; i < m_treeCount; ++i)
	{
		// Set new tree position
		glm::vec3 treePosition = m_terrain.GetVertexPosition(rand() % terrainSize);
		m_pTree[i].SetPosition(treePosition);

		// Set tree model
		unsigned int treeModel = rand() % 100;

		if (treeModel > 50)
		{
			m_pTree[i].SetModel(&m_pTreeModel[0]);
		}
		else
		{
			m_pTree[i].SetModel(&m_pTreeModel[1]);
		}
	}

	// Set particile system's postion to terrain peak
	glm::vec3 peakPosition;
	for (unsigned int i = 0; i < terrainSize; ++i)
	{
		glm::vec3 vertexPosition = m_terrain.GetVertexPosition(i);

		if (vertexPosition[1] > peakPosition[1])
			peakPosition = vertexPosition;
	}

	m_particleSystem.SetPosition(peakPosition + glm::vec3(0.f, 5.f, 0.f));
}

void Game1::DrawTrees()
{
	for (unsigned int i = 0; i < m_treeCount; ++i)
	{
		m_pTree[i].Draw(m_camera, m_dirLight);
	}
}

void Game1::Draw(const float &a_deltaTime)
{
	glm::mat4 projectionView = m_camera.GetProjectionViewTransform();

	// Increment frame counter
	m_frameCounter++;

	// Clear screen render target
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (m_wireFrame)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	// Draw terrain
	glEnable(GL_CULL_FACE);
	m_terrain.Draw(m_camera, m_dirLight);

	// Draw particle system
	m_particleSystem.Draw(m_camera, m_dirLight);

	// Draw trees
	glDisable(GL_CULL_FACE);
	DrawTrees();

	// Render Gizmos
	Gizmos::clear();

	if (m_drawTerrainNorm)
		m_terrain.DrawNormals();

	Gizmos::draw(projectionView);

	// Render tweak bar
	if (m_showTweakBar)
	{
		if (m_wireFrame)
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		m_pTweakBar->Draw();
	}
}

void Game1::Shutdown()
{
	delete m_pTweakBar;
	delete[] m_pTree;
	delete[] m_pTreeModel;
}

Game1::~Game1()
{
}