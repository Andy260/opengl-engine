#include "Tree.h"
#include <glm\ext.hpp>
#include <Camera\Camera.h>
#include <Render\Lighting\DirectionalLight.h>
#include <Content\Model\FBXModel.h>


Tree::Tree()
{
	// Set scale and roation
	m_worldTransform = glm::scale(glm::vec3(0.1f, 0.1f, 0.1f));
	m_worldTransform = glm::rotate(m_worldTransform, glm::pi<float>() / 2, glm::vec3(-1, 0, 0));
}

void Tree::SetPosition(const glm::vec3& a_position)
{
	m_worldTransform[3] = glm::vec4(a_position, 1);
}

void Tree::Draw(const Camera& a_camera, const DirectionalLight& a_dirLight)
{
	m_pModel->SetTransform(m_worldTransform);

	m_pModel->Draw(a_camera, a_dirLight);
}