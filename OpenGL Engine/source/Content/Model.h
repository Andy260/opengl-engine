#pragma once

class FBXModel;
class OBJModel;

class Model
{
private:
	FBXModel* m_fbxModel;
	OBJModel* m_objModel;

public:
	Model();
	~Model();

	void Draw();
};