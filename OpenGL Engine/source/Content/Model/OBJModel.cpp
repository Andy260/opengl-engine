#include "OBJModel.h"
#include <iostream>
#include <gl_core_4_4.h>


OBJModel::~OBJModel()
{
	for (unsigned int i = 0; i < m_glHandles.size(); ++i)
	{
		glDeleteBuffers(1, &m_glHandles[i].m_VBO);
		glDeleteBuffers(1, &m_glHandles[i].m_IBO);
		glDeleteVertexArrays(1, &m_glHandles[i].m_VAO);
	}
}

void OBJModel::Load(const char* a_filePath)
{
	// Load model and check for errors
	std::string error = tinyobj::LoadObj(m_shapes, m_materials, a_filePath);
	if (error.length() > 0)
	{
		std::cerr << "Model Load Error: " << error << std::endl;
	}

	// Allocate space within list for all GL handles
	m_glHandles.resize(m_shapes.size());

	for (unsigned int meshIndex = 0; meshIndex < m_shapes.size(); ++meshIndex)
	{
		// Generate buffers for this shape
		glGenVertexArrays(1, &m_glHandles[meshIndex].m_VAO);
		glGenBuffers(1, &m_glHandles[meshIndex].m_VBO);
		glGenBuffers(1, &m_glHandles[meshIndex].m_IBO);
		glBindVertexArray(m_glHandles[meshIndex].m_VAO);

		// Get vertex data size
		unsigned int bufferSize = m_shapes[meshIndex].mesh.positions.size();
		bufferSize += m_shapes[meshIndex].mesh.normals.size();
		bufferSize += m_shapes[meshIndex].mesh.texcoords.size();

		// Allocate vector for vertex data, and populate with data
		std::vector<float> vertexData;
		vertexData.reserve(bufferSize);
		// Insert positions
		vertexData.insert(vertexData.end(),
			m_shapes[meshIndex].mesh.positions.begin(),
			m_shapes[meshIndex].mesh.positions.end());
		// Insert normals
		vertexData.insert(vertexData.end(),
			m_shapes[meshIndex].mesh.normals.begin(),
			m_shapes[meshIndex].mesh.normals.end());

		// Set index count for this mesh
		m_glHandles[meshIndex].m_indexCount = m_shapes[meshIndex].mesh.indices.size();

		// Send data to buffer
		glBindBuffer(GL_ARRAY_BUFFER, m_glHandles[meshIndex].m_VBO);
		glBufferData(GL_ARRAY_BUFFER, vertexData.size() * sizeof(float),
			vertexData.data(), GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_glHandles[meshIndex].m_IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
			m_shapes[meshIndex].mesh.indices.size() * sizeof(unsigned int),
			m_shapes[meshIndex].mesh.indices.data(), GL_STATIC_DRAW);

		// Enable shader attributes, and tell GL how to use the buffer data
		glEnableVertexAttribArray(0); // Positions
		glEnableVertexAttribArray(1); // Normals
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 0, (void*)(sizeof(float)*m_shapes[meshIndex].mesh.positions.size()));
	}

	// Unbind buffers
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void OBJModel::Draw()
{
	// Draw model
	for (unsigned int i = 0; i < m_glHandles.size(); ++i)
	{
		glBindVertexArray(m_glHandles[i].m_VAO);
		glDrawElements(GL_TRIANGLES, m_glHandles[i].m_indexCount, GL_UNSIGNED_INT, 0);
	}
}