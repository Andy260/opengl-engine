#include "FBXFileLoader.h"
#include <fbxsdk.h>


bool FBXFileLoader::m_isCreated(false);
FbxManager* FBXFileLoader::m_pFbxManager(nullptr);
FbxIOSettings* FBXFileLoader::m_pFbxIOSettings(nullptr);
FbxImporter* FBXFileLoader::m_pFbxImporter(nullptr);

bool FBXFileLoader::Create()
{
	if (!m_isCreated)
	{
		m_pFbxManager = FbxManager::Create();

		if (!m_pFbxManager)
			return false;	// FBX Manager creation error

		m_pFbxIOSettings = FbxIOSettings::Create(m_pFbxIOSettings, IOSROOT);
		m_pFbxManager->SetIOSettings(m_pFbxIOSettings);

		// TODO: Configure Fbx IO settings

		m_isCreated = true;
	}

	return true;
}

void FBXFileLoader::Destroy()
{
	if (m_pFbxManager != nullptr)
		m_pFbxManager->Destroy();

	m_isCreated = false;
}