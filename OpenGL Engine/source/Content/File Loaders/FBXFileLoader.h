#pragma once
#include <string>
#include <fbxsdk.h>

class Model;

class FBXFileLoader
{
private:
	static bool m_isCreated;
	static FbxManager* m_pFbxManager;
	static FbxIOSettings* m_pFbxIOSettings;
	static FbxImporter* m_pFbxImporter;

	FBXFileLoader(){}
	~FBXFileLoader(){}

public:

	Model LoadModel(std::string a_path);
	static bool Create();
	static void Destroy();
};