#pragma once
#include <glm\glm.hpp>


class Camera
{
protected:
	// Transform within the worldspace (position, rotation)
	glm::mat4 m_worldTransform;
	// Viewspace transform (moves world to make camera the world origin)
	glm::mat4 m_viewTransform;
	// Contains aspect ratio, fov and cliping space
	glm::mat4 m_projectionTransform;
	// Screenspace transform
	glm::mat4 m_projectionViewTransform;

	// Updates the (projection * view) transform 
	// when world transform has changed
	void UpdateProjectionViewTransforms();

public:
	Camera();
	Camera(const glm::mat4 &a_transform);
	Camera(const glm::mat4 &a_transform, const float &a_fovY, 
		   const float &a_aspect, const float &a_nearClip, 
		   const float &a_farClip);

	void LookAt(const glm::vec3 &a_eyeDir, const glm::vec3 &a_worldUp);
	void LookAt(const glm::vec3 &a_pos, const glm::vec3 &a_eyeDir,
				const glm::vec3 &a_worldUp);

	void SetTransform(const glm::mat4 &a_trans);
	void SetPosition(const glm::vec3 &a_pos);
	void SetProjection(const float &a_fovY, const float &a_aspect, 
		const float &a_nearClip, const float &a_farClip);

	inline glm::vec3 GetPosition() const { return m_worldTransform[3].xyz; }
	inline glm::mat4 GetTransform()	const { return m_worldTransform; }
	inline glm::mat4 GetViewTransform()	const { return m_viewTransform; }
	inline glm::mat4 GetProjectionTransform() const { return m_projectionTransform; }
	inline glm::mat4 GetProjectionViewTransform() const { return m_projectionViewTransform; }
};