#include "Camera.h"
#include <glm\ext.hpp>


Camera::Camera()
{
	m_worldTransform = glm::mat4(1);
	m_viewTransform = glm::mat4(1);
}
Camera::Camera(const glm::mat4 &a_transform)
{

}
Camera::Camera(const glm::mat4 &a_transform, const float &a_fovY, 
			   const float &a_aspect, const float &a_nearClip, 
			   const float &a_farClip)
{
	
}

void Camera::UpdateProjectionViewTransforms()
{
	m_viewTransform				= glm::inverse(m_worldTransform);
	m_projectionViewTransform   = m_projectionTransform * m_viewTransform;
}

void Camera::LookAt(const glm::vec3 &a_lookAt, const glm::vec3 &a_worldUp)
{
	// Rotates camera
	LookAt(m_worldTransform[3].xyz, a_lookAt, a_worldUp);
}
void Camera::LookAt(const glm::vec3 &a_pos, const glm::vec3 &a_lookAt, 
				const glm::vec3 &a_worldUp)
{
	// Set new viewSpace transform and updates world and projection transforms
	m_worldTransform = glm::inverse(glm::lookAt(a_pos, a_lookAt, a_worldUp));
	UpdateProjectionViewTransforms();
}

void Camera::SetTransform(const glm::mat4 &a_trans)
{
	// Sets new transform, and updates view and projection transforms
	m_worldTransform = a_trans;
	UpdateProjectionViewTransforms();
}

void Camera::SetPosition(const glm::vec3 &a_pos)
{
	// Sets position within world transform, and updates view transform
	m_worldTransform[3] = glm::vec4(a_pos, 1);
	UpdateProjectionViewTransforms();
}

void Camera::SetProjection(const float &a_fovY, const float &a_aspect, 
		const float &a_nearClip, const float &a_farClip)
{
	 // Creates new projection transform
	 m_projectionTransform = glm::perspective(a_fovY, a_aspect, a_nearClip, a_farClip);
	 UpdateProjectionViewTransforms();
}