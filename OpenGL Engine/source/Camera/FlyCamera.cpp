#include "FlyCamera.h"
#include <Input\Keyboard.h>
#include <Input\Mouse.h>
#include <GameWindow.h>
#include <glm\ext.hpp>


FlyCamera::FlyCamera(GameWindow* a_pWindow)
	: m_speed(0.0f), m_rotating(false),
	m_pWindow(a_pWindow)
{
	
}

void FlyCamera::Update(const float &a_deltaTime)
{
	HandleKeyboardInput(a_deltaTime);
	HandleMouseInput();
}

void FlyCamera::HandleKeyboardInput(const float& a_deltaTime)
{
	glm::vec3 right = m_worldTransform[0].xyz;
	glm::vec3 up = m_worldTransform[1].xyz;
	glm::vec3 forward = m_worldTransform[2].xyz;

	glm::vec3 translation = glm::vec3(0.0f);
	// Handle Z translation
	if (Keyboard::IsKeyDown(Keys::KEY_W))
	{
		translation -= forward;
	}
	if (Keyboard::IsKeyDown(Keys::KEY_S))
	{
		translation += forward;
	}
	// Handle X translation
	if (Keyboard::IsKeyDown(Keys::KEY_A))
	{
		translation -= right;
	}
	if (Keyboard::IsKeyDown(Keys::KEY_D))
	{
		translation += right;
	}
	// Handle Y translation
	if (Keyboard::IsKeyDown(Keys::KEY_LEFT_SHIFT))
	{
		translation += glm::vec3(0.0f, 1.0f, 0.0f);
	}
	if (Keyboard::IsKeyDown(Keys::KEY_LEFT_CONTROL))
	{
		translation -= glm::vec3(0.0f, 1.0f, 0.0f);
	}

	// Apply translation
	float length = glm::length(translation);
	if (length > 0.01f)
	{
		translation = (a_deltaTime * m_speed) * glm::normalize(translation);
		SetPosition(GetPosition() + translation);
		UpdateProjectionViewTransforms();
	}
}

void FlyCamera::HandleMouseInput()
{
	// Handle Mouse Input
	int screenWidth, screenHeight = 0;
	m_pWindow->GetFrameBufferSize(screenWidth, screenHeight);
	glm::vec2 mouseOffset(screenWidth / 2, screenHeight / 2);

	if (Mouse::IsButtonDown(MouseButton::RIGHT))
	{
		if (!m_rotating)
		{
			Mouse::InputMode(MouseMode::DISABLED);
			m_oldMousePos = Mouse::GetPosition();
			Mouse::SetPosition(mouseOffset);
			m_rotating = true;
		}
		else
		{
			glm::vec2 mousePos = Mouse::GetPosition();
			Rotate((mousePos - mouseOffset) * 0.005f);

			Mouse::SetPosition(mouseOffset);
		}
	}
	else
	{
		if (m_rotating)
		{
			Mouse::SetPosition(m_oldMousePos);
			Mouse::InputMode(MouseMode::VISIBLE);
			m_rotating = false;
		}
	}
}

void FlyCamera::Translate(const glm::vec3 &a_offset)
{
	m_worldTransform[3].x += a_offset.x;
	m_worldTransform[3].y += a_offset.y;
	m_worldTransform[3].z += a_offset.z;

	void UpdateProjectionViewTransforms();
}

void FlyCamera::Rotate(const glm::vec2 &a_pitchYaw)
{
	// TODO: Limit Y Rotation to no more than 90 degrees

	// Rotation XYZ
	glm::vec3 right = glm::vec3(1, 0, 0);
	glm::vec3 up = glm::vec3(0, 1, 0);
	glm::vec3 forward = m_worldTransform[2].xyz;

	// Rotation matricies
	glm::mat4 xRotation = glm::rotate(-a_pitchYaw.x, up);
	glm::mat4 yRotation = glm::rotate(-a_pitchYaw.y, right);

	// Apply rotation to world transform
	m_worldTransform = m_worldTransform * xRotation * yRotation;

	// Cross product vectors
	right = glm::cross(glm::vec3(0, 1, 0), m_worldTransform[2].xyz());
	up = glm::cross(m_worldTransform[2].xyz(), right);

	// Normalise Vectors to remove strange values
	right = glm::normalize(right);
	up = glm::normalize(up);

	// Add new vectors into world transform
	m_worldTransform[0] = glm::vec4(right, 0);
	m_worldTransform[1] = glm::vec4(up, 0);

	UpdateProjectionViewTransforms();
}