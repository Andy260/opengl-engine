#include "GameWindow.h"
#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>
#include <AntTweakBar.h>


GameWindow::GameWindow(const int &a_width, const int &a_height,
	const std::string &a_title, const bool &a_fullscreen)
{
	// Handle full screen window creation
	GLFWmonitor* pMoniter = nullptr;
	if (a_fullscreen)
		pMoniter = glfwGetPrimaryMonitor();

	// Create window
	m_pGLFWhandle = glfwCreateWindow(a_width, a_height, a_title.c_str(), 
									 pMoniter, nullptr);

	glfwMakeContextCurrent(m_pGLFWhandle);

	glfwSetWindowSizeCallback(m_pGLFWhandle, 
		[](GLFWwindow* a_pWindow, int a_width, int a_height) -> void
	{
		glfwSetWindowSize(a_pWindow, a_width, a_height);

		// Reset GL viewport
		int width, height = 0;
		glfwGetFramebufferSize(a_pWindow, &width, &height);
		glViewport(0, 0, width, height);
	});
}

GameWindow::~GameWindow()
{
	glfwDestroyWindow(m_pGLFWhandle);
}

bool GameWindow::Initialise()
{
	// Initialise GLFW
	if (glfwInit() == false)
		return false;

	return true;
}

int GameWindow::ShouldClose()
{
	return glfwWindowShouldClose(m_pGLFWhandle);
}

void GameWindow::SwapBuffers()
{
	glfwSwapBuffers(m_pGLFWhandle);
}

void GameWindow::GetSize(int* a_pWidth, int* a_pHeight)
{
	glfwGetWindowSize(m_pGLFWhandle, a_pWidth, a_pHeight);
}

void GameWindow::SetSize(const int &a_width, const int &a_height)
{
	glfwSetWindowSize(m_pGLFWhandle, a_width, a_height);

	// Reset GL viewport
	int width, height = 0;
	GetFrameBufferSize(width, height);
	glViewport(0, 0, width, height);
}

void GameWindow::SetTitle(const std::string &a_title)
{
	glfwSetWindowTitle(m_pGLFWhandle, a_title.c_str());
}

void GameWindow::GetFrameBufferSize(int& a_width, int& a_height)
{
	glfwGetFramebufferSize(m_pGLFWhandle, &a_width, &a_height);
}