#pragma once
#include <vector>
#include <list>
#include <glm\vec3.hpp>

class BoundingSphere;
class BoundingFrustum;
class Plane;

// Defines axis aligned bounding box
class BoundingBox
{
private:
	glm::vec3 m_min, m_max;

public:
	BoundingBox(const glm::vec3& a_min, const glm::vec3& a_max);

	void Fit(const std::vector<glm::vec3>& a_pointsList);
	void Fit(const std::vector<glm::vec3*>& a_pointsList);
	void Fit(const std::list<glm::vec3>& a_pointsList);
	void Fit(const std::list<glm::vec3*>& a_pointsList);

	bool Intersects(const BoundingBox& a_box);
	bool Intersects(const BoundingSphere& a_sphere);
	bool Intersects(const BoundingFrustum& a_frustum);
	bool Intersects(const Plane& a_plane);

	inline glm::vec3 GetMin() const { return m_min; }
	inline glm::vec3 GetMax() const { return m_max; }

	inline void SetMin(const glm::vec3& a_min) { m_min = a_min; }
	inline void SetMax(const glm::vec3& a_max) { m_max = a_max; }
};