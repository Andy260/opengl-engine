#pragma once
#include <glm\vec3.hpp>
#include <glm\vec4.hpp>

class BoundingSphere;
class BoundingBox;
class BoundingFrustum;

enum class PlaneIntersection
{
	FRONT,
	BACK,
	INTERSECT
};

class Plane
{
	glm::vec3 m_normal;
	float m_distance;

	//glm::vec4 m_normalD;

public:
	Plane(const glm::vec4& a_value);
	Plane(const glm::vec3& a_normal, const float& a_distance);

	void Normalise();

	PlaneIntersection Intersects(const BoundingSphere& a_sphere);
	PlaneIntersection Intersects(const BoundingBox& a_box);
	PlaneIntersection Intersects(const BoundingFrustum& a_frustum);

	inline glm::vec3 GetNormal() const { return m_normal; }
	inline float GetDistance() const { return m_distance; }
};