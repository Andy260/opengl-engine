#include "Mouse.h"
#include <GameWindow.h>
#include <GLFW/glfw3.h>
#include <AntTweakBar.h>


GameWindow* Mouse::m_pWindow = nullptr;
void Mouse::Intialise(GameWindow *a_pWindow)
{
	m_pWindow = a_pWindow;

	// Set AntTweak bar callbacks
	glfwSetMouseButtonCallback(m_pWindow->m_pGLFWhandle, 
		[] (GLFWwindow*, int b, int a, int m) 
	{
		TwEventMouseButtonGLFW(b, a);
	});

	glfwSetCursorPosCallback(m_pWindow->m_pGLFWhandle, 
		[] (GLFWwindow*, double x, double y) 
	{
		TwEventMousePosGLFW((int)x, (int)y);
	});

	glfwSetScrollCallback(m_pWindow->m_pGLFWhandle, 
		[](GLFWwindow*, double x, double y) 
	{
		TwEventMouseWheelGLFW((int)y);
	});
}

bool Mouse::IsButtonDown(const MouseButton &a_button)
{
	if (glfwGetMouseButton(
		m_pWindow->m_pGLFWhandle, (int)a_button) == GLFW_PRESS)
		return true;
	else
		return false;
}

bool Mouse::IsButtonUp(const MouseButton &a_button)
{
	if (glfwGetMouseButton(
		m_pWindow->m_pGLFWhandle, (int)a_button) == GLFW_RELEASE)
		return true;
	else
		return false;
}

glm::vec2 Mouse::GetPosition()
{
	double x;
	double y;
	glfwGetCursorPos(m_pWindow->m_pGLFWhandle, &x, &y);

	return glm::vec2((float)x, (float)y);
}

void Mouse::SetPosition(const glm::vec2 &a_pos)
{
	glfwSetCursorPos(m_pWindow->m_pGLFWhandle, 
		(double)a_pos.x, (double)a_pos.y);
}

void Mouse::InputMode(const MouseMode& a_mode)
{
	int mode = 0;

	switch (a_mode)
	{
	case MouseMode::VISIBLE:
		mode = GLFW_CURSOR_NORMAL;
		break;
	case MouseMode::INVISIBLE:
		mode = GLFW_CURSOR_HIDDEN;
		break;
	case MouseMode::DISABLED:
		mode = GLFW_CURSOR_HIDDEN;
		break;
	}

	glfwSetInputMode(m_pWindow->m_pGLFWhandle, GLFW_CURSOR, mode);
}