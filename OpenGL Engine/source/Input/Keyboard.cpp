#include "Keyboard.h"
#include <GameWindow.h>
#include <GLFW/glfw3.h>
#include <AntTweakBar.h>


GameWindow* Keyboard::m_pWindow = nullptr;
void Keyboard::Intialise(GameWindow *a_pWindow)
{
	m_pWindow = a_pWindow;

	// Set AntTweak bar callbacks
	glfwSetKeyCallback(m_pWindow->m_pGLFWhandle, 
		[] (GLFWwindow*, int k, int s, int a, int m) 
	{
		TwEventKeyGLFW(k, a);
	});

	glfwSetCharCallback(m_pWindow->m_pGLFWhandle, 
		[](GLFWwindow*, unsigned int c) 
	{
		TwEventCharGLFW(c, GLFW_PRESS);
	});
}

bool Keyboard::IsKeyDown(const Keys &a_key)
{
	// Check if key is being pressed, via GLFW
	if (glfwGetKey(m_pWindow->m_pGLFWhandle, (int)a_key) == GLFW_PRESS)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Keyboard::IsKeyUp(const Keys &a_key)
{
	// Check if key is not being pressed, via GLFW
	if (glfwGetKey(m_pWindow->m_pGLFWhandle, (int)a_key) == GLFW_RELEASE)
	{
		return true;
	}
	else
	{
		return false;
	}
}