#pragma once
#include <glm\vec2.hpp>

class GameWindow;

// Buttons on mouse
enum MouseButton
{
	LEFT = 0,
	RIGHT = 1,
	MIDDLE = 2,
	FOUR = 3,
	FIVE = 4,
	SIX = 5,
	SEVEN = 6,
	EIGHT = 7
};

enum class MouseMode
{
	VISIBLE = 0,
	INVISIBLE = 1,
	DISABLED = 2
};

class Mouse
{
private:
	// Pointer to game window to check input against
	static GameWindow *m_pWindow;

public:
	// Sets window to check input upon
	static void Intialise(GameWindow *window);

	// Checks if button is currently being pressed
	static bool IsButtonDown(const MouseButton &a_button);

	// Checks if button is currently not being pressed
	static bool IsButtonUp(const MouseButton &a_button);

	// Returns position of mouse
	static glm::vec2 GetPosition();

	// Sets position of mouse
	static void SetPosition(const glm::vec2 &a_pos);

	// Sets the input mode
	static void InputMode(const MouseMode& a_mode);
};