#include "Plane.h"
#include <BoundingBox.h>
#include <BoundingFrustum.h>
#include <BoundingSphere.h>
#include <glm\glm.hpp>


Plane::Plane(const glm::vec4& a_value)
	: m_normal(a_value.xyz), m_distance(a_value.w)
{
	
}

Plane::Plane(const glm::vec3& a_normal, const float& a_distance)
	: m_normal(a_normal), m_distance(a_distance)
{

}

void Plane::Normalise()
{
	glm::vec4 temp(m_normal, m_distance);
	temp = glm::normalize(temp);

	m_normal = temp.xyz;
	m_distance = temp.z;
}

PlaneIntersection Plane::Intersects(const BoundingSphere& a_sphere)
{
	float dot = glm::dot(glm::vec3(m_normal), a_sphere.GetCentre() + m_distance);

	if (dot > a_sphere.GetRadius())
		return PlaneIntersection::FRONT;
	else if (dot < a_sphere.GetRadius())
		return PlaneIntersection::BACK;
	else
		return PlaneIntersection::INTERSECT;
}

PlaneIntersection Plane::Intersects(const BoundingBox& a_box)
{
	// AABB World space tranformation
	// http://www.gamedev.net/topic/349370-transform-aabb-from-local-to-world-space-for-frustum-culling/

	return PlaneIntersection::BACK;
}

PlaneIntersection Plane::Intersects(const BoundingFrustum& a_frustum)
{
	return PlaneIntersection::BACK;
}