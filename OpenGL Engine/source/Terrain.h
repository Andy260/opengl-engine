#pragma once
#include <vector>
#include <Render\Shader.h>
#include <glm\mat4x4.hpp>
#include <glm\vec2.hpp>
#include <glm\vec3.hpp>
#include <glm\vec4.hpp>

class DirectionalLight;
class Texture2D;
class Camera;

class Terrain
{
private:
	struct Vertex
	{
		glm::vec4 position;
		glm::vec3 normal;
		glm::vec2 texCoord;
	};
	Vertex* m_vertexData;
	unsigned int* m_indiciesData;

	Texture2D* m_pDiffuseTex;

	// OpenGL handles
	unsigned int m_vao, m_vbo, m_ibo;

	float m_seed;

	unsigned int m_size; // Size of terrain

	// Perlin noise variables
	float m_frequency, m_amplitude, m_persistence, m_octaves;

	Shader m_shader;

	void PerlinNoiseExample(float* a_pData, const int& a_size, const float& a_seed);

	void GenerateNormal(Vertex* a_vert1, Vertex* a_vert2, Vertex* a_vert3);

	void GeneratePlaneData(Vertex* a_pVertexData, unsigned int* a_pIndices);
	void CreateGLBuffers(Vertex* a_pVertexData, unsigned int* a_pIndices);

public:
	Terrain(const unsigned int& a_size);
	~Terrain();

	void Initialise();

	void Generate();

	void Draw(const Camera& a_camera, 
			  const DirectionalLight& a_dirLight);
	void DrawNormals();

	glm::vec3 GetVertexPosition(const unsigned int& a_index) const;

	inline unsigned int GetSize() const { return m_size; }
	inline float GetFrequency() const { return m_frequency; }
	inline float GetAmplitude() const { return m_amplitude; }
	inline float GetPersistence() const { return m_persistence; }
	inline float GetOctaves() const { return m_octaves; }
	inline float GetSeed() const { return m_seed; }

	inline void SetSize(const unsigned int& a_size) { m_size = a_size; }
	inline void SetFrequency(const float& a_frequency) { m_frequency = a_frequency; }
	inline void SetAmplitude(const float& a_amplitude) { m_amplitude = a_amplitude; }
	inline void SetPersistence(const float& a_persistence) { m_persistence = a_persistence; }
	inline void SetOctaves(const float& a_octaves) { m_octaves = a_octaves; }
	inline void SetSeed(const float& a_seed) { m_seed = a_seed; }
};