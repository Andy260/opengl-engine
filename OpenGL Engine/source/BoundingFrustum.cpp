#include "BoundingFrustum.h"
#include <BoundingBox.h>
#include <BoundingSphere.h>
#include <Plane.h>
#include <glm\glm.hpp>


BoundingFrustum::BoundingFrustum(const glm::mat4& a_projectionView)
	: m_near(glm::vec4(a_projectionView[0][3] + a_projectionView[0][2],
					   a_projectionView[1][3] + a_projectionView[1][2],
					   a_projectionView[2][3] + a_projectionView[2][2],
					   a_projectionView[3][3] + a_projectionView[3][2])),

	m_far(glm::vec4(a_projectionView[0][3] - a_projectionView[0][2],
					a_projectionView[1][3] - a_projectionView[1][2],
					a_projectionView[2][3] - a_projectionView[2][2],
					a_projectionView[3][3] - a_projectionView[3][2])),

	m_top(glm::vec4(a_projectionView[0][3] - a_projectionView[0][1],
					a_projectionView[1][3] - a_projectionView[1][1],
					a_projectionView[2][3] - a_projectionView[2][1],
					a_projectionView[3][3] - a_projectionView[3][1])),

	m_bottom(glm::vec4(a_projectionView[0][3] + a_projectionView[0][1],
					   a_projectionView[1][3] + a_projectionView[1][1],
					   a_projectionView[2][3] + a_projectionView[2][1],
					   a_projectionView[3][3] + a_projectionView[3][1])),

	m_left(glm::vec4(a_projectionView[0][3] + a_projectionView[0][0],
					 a_projectionView[1][3] + a_projectionView[1][0],
					 a_projectionView[2][3] + a_projectionView[2][0],
					 a_projectionView[3][3] + a_projectionView[3][0])),

	m_right(glm::vec4(a_projectionView[0][3] - a_projectionView[1][0],
					  a_projectionView[1][3] - a_projectionView[1][0],
					  a_projectionView[2][3] - a_projectionView[2][0],
					  a_projectionView[3][3] - a_projectionView[3][0]))
{
	m_near.Normalise();
	m_far.Normalise();
	m_top.Normalise();
	m_bottom.Normalise();
	m_left.Normalise();
	m_right.Normalise();
}

bool BoundingFrustum::Contains(const BoundingSphere& a_sphere)
{
	return false;
}

bool BoundingFrustum::Contains(const BoundingBox& a_box)
{
	return false;
}

bool BoundingFrustum::Intersects(const BoundingSphere& a_sphere)
{
	return false;
}

bool BoundingFrustum::Intersects(const BoundingBox& a_box)
{
	return false;
}