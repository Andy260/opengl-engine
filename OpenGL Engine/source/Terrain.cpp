#include "Terrain.h"
#include <gl_core_4_4.h>
#include <glm\glm.hpp>
#include <glm\ext.hpp>
#include <Gizmos.h>
#include <Content\Texture2D.h>
#include <Camera\Camera.h>
#include <Render\Lighting\DirectionalLight.h>


Terrain::Terrain(const unsigned int& a_size)
	: m_vao(0),
	m_vbo(0),
	m_ibo(0),
	m_size(a_size),
	m_frequency(2.0f),
	m_amplitude(1.0f),
	m_persistence(0.3f),
	m_octaves(6.0f)
{
	// Create shader
	m_shader.AddShaderFile("./shaders/terrain.vert", ShaderStage::VERTEX);
	m_shader.AddShaderFile("./shaders/terrain.frag", ShaderStage::FRAGMENT);
	m_shader.LinkProgram();

	// Generate GL buffers
	glGenBuffers(1, &m_vbo);
	glGenBuffers(1, &m_ibo);
	glGenVertexArrays(1, &m_vao);
}

Terrain::~Terrain()
{
	glDeleteBuffers(1, &m_vbo);
	glDeleteBuffers(1, &m_ibo);
	glDeleteVertexArrays(1, &m_vao);

	delete[] m_vertexData;
	delete[] m_indiciesData;
	delete m_pDiffuseTex;
}

void Terrain::Initialise()
{
	// Generate initial plane data
	m_vertexData = new Vertex[m_size * m_size];
	m_indiciesData = new unsigned int[(m_size - 1) *
		(m_size - 1) * 6];

	GeneratePlaneData(m_vertexData, m_indiciesData);
	CreateGLBuffers(m_vertexData, m_indiciesData);

	m_pDiffuseTex = new Texture2D("./textures/terrain/grass.jpg");
}

void Terrain::PerlinNoiseExample(float* a_pData, const int& a_size, const float& a_seed)
{
	float scale = (1.0f / (float)m_size) * 3.0f;
	float octaves = m_octaves;

	float heightmin = 1e37f;
	float heightMax = 0;
	for (int x = 0; x < (int)m_size; ++x)
	{
		for (int y = 0; y < (int)m_size; ++y)
		{
			float amplitude = m_amplitude;
			float persistence = m_persistence;
			a_pData[y * m_size + x] = 0;

			for (int o = 0; o < octaves; ++o)
			{
				float freq = powf(m_frequency, (float)o);
				float perlinSample = glm::perlin(glm::vec3((float)x, a_seed, (float)y) * scale * freq) * 0.5f + 0.5f;

				float height = perlinSample * amplitude;

				a_pData[y * m_size + x] += height;
				amplitude *= persistence;

				// Save height differences for normalisation
				if (height < heightmin)
					heightmin = height;
				else if (height > heightMax)
					heightMax = height;
			}
		}
	}

	// Normalise height
	float heightDif = heightMax - heightmin;
	for (unsigned int i = 0; i < m_size * m_size; ++i)
	{
		a_pData[i] -= heightDif;
	}
}

void Terrain::Generate()
{
	// Generate perlin noise terrain transform
	unsigned int perlinData_size = m_size * m_size;
	float* perlinData = new float[perlinData_size];

	PerlinNoiseExample(perlinData, perlinData_size, m_seed);

	// Apply transformation
	for (unsigned int i = 0; i < perlinData_size; ++i)
	{
		m_vertexData[i].position.y = perlinData[i] * 5;
	}

	unsigned int indiciesSize = (m_size - 1) *
		(m_size - 1) * 6;
	// Generate new normals
	for (unsigned int i = 0; i < indiciesSize; i += 3)
	{
		Vertex* vertex1 = &m_vertexData[m_indiciesData[i + 2]];
		Vertex* vertex2 = &m_vertexData[m_indiciesData[i + 1]];
		Vertex* vertex3 = &m_vertexData[m_indiciesData[i]];

		GenerateNormal(vertex1, vertex2, vertex3);
	}

	// Update GPU data
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_size * m_size *
		sizeof(Vertex), m_vertexData);

	// Clean up
	delete[] perlinData;
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Terrain::GenerateNormal(Vertex* a_vert1, Vertex* a_vert2, Vertex* a_vert3)
{
	// Calculate face normal
	glm::vec3 d1(a_vert3->position - a_vert1->position);
	glm::vec3 d2(a_vert2->position - a_vert1->position);

	glm::vec3 crossProduct = glm::cross(d1, d2);
	
	// Normalise normal
	glm::vec3 normal = glm::normalize(crossProduct);

	a_vert1->normal = normal;
	a_vert2->normal = normal;
	a_vert3->normal = normal;
}

void Terrain::CreateGLBuffers(Vertex* a_pVertexData, unsigned int* a_pIndices)
{
	// Bind Vertex Array
	glBindVertexArray(m_vao);

	// Populate VBO
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, (m_size * m_size) * sizeof(Vertex),
		a_pVertexData, GL_DYNAMIC_DRAW);

	// Setup Attribute Location 0 - Position
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex),
		0);

	// Setup Attribute Location 1 - Normals
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, sizeof(Vertex),
		(void*)(sizeof(glm::vec4)));

	// Setup Attribute Location 2 - TexCoord
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
		(void*)(sizeof(glm::vec4) + sizeof(glm::vec3)));

	// Populate IBO
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		(m_size - 1) * (m_size - 1) * 6 * sizeof(unsigned int),
		a_pIndices, GL_STATIC_DRAW);

	// Clean up
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Terrain::GeneratePlaneData(Vertex* a_pVertexData, unsigned int* a_pIndices)
{
	for (unsigned int row = 0; row < m_size; ++row)
	{
		for (unsigned int collum = 0; collum < m_size; ++collum)
		{
			// Position
			glm::vec4 position((float)collum - ((float)m_size / 2.f), 0, 
				(float)row - ((float)m_size / 2.f), 1);

			// Texture coordinates
			glm::vec2 texCoord((float)collum, (float)row);

			// Normals
			glm::vec3 normal(0, 1, 0);

			a_pVertexData[row * m_size + collum].position	= position;
			a_pVertexData[row * m_size + collum].texCoord	= texCoord;
			a_pVertexData[row * m_size + collum].normal		= normal;
		}
	}

	unsigned int index = 0;
	for (unsigned int row = 0; row < (m_size - 1); ++row)
	{
		for (unsigned int collum = 0; collum < (m_size - 1); ++collum)
		{
			// Triangle 1
			int currVert = row * m_size + collum;
			a_pIndices[index++] = currVert;
			a_pIndices[index++] = currVert + m_size;
			a_pIndices[index++] = currVert + m_size + 1;

			// Triangle 2
			a_pIndices[index++] = currVert;
			a_pIndices[index++] = currVert + m_size + 1;
			a_pIndices[index++] = currVert + 1;
		}
	}
}

void Terrain::Draw(const Camera& a_camera, 
				   const DirectionalLight& a_dirLight)
{
	m_shader.Bind();

	// Pass through projection view matrix to shader
	int uniform = m_shader.GetUniform("projectionView");
	glUniformMatrix4fv(uniform, 1, GL_FALSE, &a_camera.GetProjectionViewTransform()[0][0]);

	// Update normal matrix
	glm::mat3 normalMatrix = glm::inverseTranspose(
		glm::mat3(a_camera.GetViewTransform()));
	uniform = m_shader.GetUniform("normalMat");
	glUniformMatrix3fv(uniform, 1, GL_FALSE, &normalMatrix[0][0]);

	// Set material
	uniform = m_shader.GetUniform("material.ambient");
	glUniform4fv(uniform, 1, &glm::vec4(1.f, 1.f, 1.f, 1.f)[0]);

	uniform = m_shader.GetUniform("material.diffuse");
	glUniform4fv(uniform, 1, &glm::vec4(1.f, 1.f, 1.f, 1.f)[0]);

	uniform = m_shader.GetUniform("material.specular");
	glUniform4fv(uniform, 1, &glm::vec4(1.f, 1.f, 1.f, 1.f)[0]);

	m_pDiffuseTex->Bind(0);
	uniform = m_shader.GetUniform("material.diffuseTex");
	glUniform1i(uniform, 0);

	// Pass through Directional Light properties
	glm::vec3 lightDir = -a_dirLight.GetDirection();
	uniform = m_shader.GetUniform("dirLight.direction");
	glUniform3fv(uniform, 1, &lightDir[0]);

	uniform = m_shader.GetUniform("dirLight.ambient");
	glUniform3fv(uniform, 1, &a_dirLight.GetColour()[0]);

	uniform = m_shader.GetUniform("dirLight.diffuse");
	glUniform3fv(uniform, 1, &a_dirLight.GetColour()[0]);

	uniform = m_shader.GetUniform("dirLight.ambientIntensity");
	glUniform1f(uniform, a_dirLight.GetAmbientIntensity());

	uniform = m_shader.GetUniform("dirLight.diffuseIntensity");
	glUniform1f(uniform, a_dirLight.GetDiffuseIntensity());

	uniform = m_shader.GetUniform("dirLight.specularIntensity");
	glUniform1f(uniform, a_dirLight.GetSpecularIntensity());

	// Pass through camera position to shader for specular highlighting
	uniform = m_shader.GetUniform("cameraPos");
	glUniform3fv(uniform, 1, &a_camera.GetPosition()[0]);

	// Draw terrain
	glBindVertexArray(m_vao);
	unsigned int indexCount = (m_size - 1) * (m_size - 1) * 6;
	glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
}

void Terrain::DrawNormals()
{
	glm::vec4 lineColour(1.0f, 0.0f, 0.0f, 1.0f);

	for (unsigned int i = 0; i < m_size * m_size; ++i)
	{
		glm::vec3 vertPos = m_vertexData[i].position.xyz;
		glm::vec3 vertNormal = m_vertexData[i].normal;

		Gizmos::addLine(vertPos, vertPos + vertNormal, lineColour);
	}
}

glm::vec3 Terrain::GetVertexPosition(const unsigned int& a_index) const
{
	glm::vec4 vertexPos = m_vertexData[a_index].position;

	glm::vec3 position = vertexPos.xyz;

	return position;
}