#include "Node.h"


Node::Node()
	: m_pParent(nullptr),
	m_dirtyWorld(false)
{
}

void Node::AddChild(Node* a_node)
{
	m_children.push_back(a_node);
}

void Node::RemoveChild(Node* a_node)
{
	m_children.remove(a_node);
}

void Node::UpdateTransforms()
{
	if (m_pParent != nullptr)
		m_pParent->UpdateTransforms();

	m_worldTransform = m_localTransform * m_pParent->m_localTransform;

	m_dirtyWorld = false;
}

glm::mat4 Node::GetWorldTransform()
{
	if (m_dirtyWorld)
		UpdateTransforms();

	return m_worldTransform;
}