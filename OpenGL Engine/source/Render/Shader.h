#pragma once
#include <string>

enum class ShaderStage
{
	VERTEX,
	TESS_CONTROL,
	TESS_EVALUATION,
	GEOMETRY,
	FRAGMENT,
	COMPUTE
};

enum class ShaderBufferMode
{
	INTERLEAVED, // Equivalant to GL_INTERLEAVED_ATTRIBS
	SEPERATE // Equivalant to GL_SEPARATE_ATTRIBS
};

class Shader
{
private:
	unsigned int m_programID;

	std::string LoadFile(const std::string &a_filePath, bool &a_status);
	bool CheckErrors(const unsigned int &a_shader);

public:
	Shader();
	~Shader();

	void AddShaderFile(const std::string &a_filepath, const ShaderStage &a_type);
	void LinkProgram();
	void Bind() const;

	unsigned int GetUniform(const std::string &a_name) const;

	// a_sepAttrib(true) = GL_SEPARATE_ATTRIBS, 
	// a_sepAttrib(false) = GL_INTERLEAVED_ATTRIBS
	void SetFeedbackVariables(const int &a_count, 
							 const char* a_variables[], 
							 const ShaderBufferMode& a_mode);
};