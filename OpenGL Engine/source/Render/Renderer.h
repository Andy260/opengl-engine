#pragma once
#include <vector>
#include <glm\vec3.hpp>

class Model;
class Texture2D;

class Renderer
{
protected:
	glm::vec3 m_clearColour;

	struct DrawBuffer
	{
		
	};
	DrawBuffer m_drawBuffer[2];

public:
	Renderer();
	~Renderer();

	void Clear(const glm::vec3& a_colour);

	void Draw();
};