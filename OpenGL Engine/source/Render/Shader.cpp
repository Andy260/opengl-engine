#include "Shader.h"
#include <iostream>
#include <fstream>
#include <gl_core_4_4.h>
#include <vector>
#include <assert.h>


Shader::Shader()
{
	m_programID = glCreateProgram();
}

Shader::~Shader()
{
	glDeleteProgram(m_programID);
}

std::string Shader::LoadFile(const std::string &a_filePath, bool &a_status)
{
	// Open file
	std::ifstream file;
	file.open((a_filePath).c_str());

	std::string output;
	std::string line;

	// Check if succeeded
	if (file.is_open())
	{
		// Read contents of file
		while (file.good())
		{
			getline(file, line);
			output.append(line + "\n");
		}
	}
	else
	{
		// Print read error
		std::cerr << "Shader load error: " << a_filePath.c_str() << std::endl;
	}

	return output;
}

bool Shader::CheckErrors(const unsigned int &a_shader)
{
	// Get error status
	int status = GL_FALSE;
	glGetShaderiv(a_shader, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE)
	{
		// Get error log
		int maxLength = 0;
		glGetShaderiv(a_shader, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<char> errorLog(maxLength);
		glGetShaderInfoLog(a_shader, maxLength, &maxLength, &errorLog[0]);

		// Print error
		std::cerr << "Shader Compiler Error: " << errorLog.data() << std::endl;
		return true;
	}

	// No errors occured
	return false;
}

void Shader::AddShaderFile(const std::string &a_filepath, const ShaderStage &a_type)
{
	// Load shader file
	bool loadFail = false;
	std::string fileString = LoadFile(a_filepath, loadFail);
	const char* file = fileString.c_str();
	if (loadFail)
		return;// Load error

	// Create shader
	unsigned int shader = 0;
	switch (a_type)
	{
	case ShaderStage::VERTEX: 
		shader = glCreateShader(GL_VERTEX_SHADER); 
		break;

	case ShaderStage::TESS_CONTROL:
		shader = glCreateShader(GL_TESS_CONTROL_SHADER);
		break;

	case ShaderStage::TESS_EVALUATION:
		shader = glCreateShader(GL_TESS_EVALUATION_SHADER);
		break;

	case ShaderStage::GEOMETRY:
		shader = glCreateShader(GL_GEOMETRY_SHADER);
		break;

	case ShaderStage::FRAGMENT:
		shader = glCreateShader(GL_FRAGMENT_SHADER);
		break;

	case ShaderStage::COMPUTE:
		shader = glCreateShader(GL_COMPUTE_SHADER);
		break;
	}

	// Pass shader to GL and compile
	glShaderSource(shader, 1, &file, NULL);
	glCompileShader(shader);
	if (CheckErrors(shader))
	{
		glDeleteShader(shader); // Compilation error...
		return;
	}

	// Link to program
	glAttachShader(m_programID, shader);
}

void Shader::LinkProgram()
{
	glLinkProgram(m_programID);

	// Check for errors
	int isLinked = 0;
	glGetProgramiv(m_programID, GL_LINK_STATUS, &isLinked);
	if (isLinked == GL_FALSE)
	{
		int maxLength = 0;
		glGetProgramiv(m_programID, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<char> errorLog(maxLength);
		glGetProgramInfoLog(m_programID, maxLength, &maxLength, &errorLog[0]);

		std::cerr << "Shader Program Link Failed: " << errorLog.data() << std::endl;

		// Recreate shader, to erase link errors
		glDeleteProgram(m_programID);
		m_programID = glCreateProgram();
	}
}

void Shader::Bind() const
{
	glUseProgram(m_programID);
}

unsigned int Shader::GetUniform(const std::string &a_name) const
{
	return glGetUniformLocation(m_programID, a_name.c_str());
}

void Shader::SetFeedbackVariables(const int &a_count,
	const char* a_variables[], const ShaderBufferMode &a_mode)
{
#ifdef DEBUG
	assert(a_count > 1 && "Shader feedback varyings count can't be less than 1!");
#endif

	if (a_mode == ShaderBufferMode::INTERLEAVED)
		glTransformFeedbackVaryings(m_programID, a_count, a_variables,
		GL_INTERLEAVED_ATTRIBS);
	else
		glTransformFeedbackVaryings(m_programID, a_count, a_variables,
		GL_SEPARATE_ATTRIBS);
}