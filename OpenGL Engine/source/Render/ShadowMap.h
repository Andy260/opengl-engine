#pragma once
#include <glm\fwd.hpp>

class Model;

class ShadowMap
{
private:
	int m_size;

	unsigned int m_fbo, m_fboDepth;

public:
	ShadowMap(const int& a_size);
	~ShadowMap();

	void Bind();
	void BindTexture(const int& a_unit);

	inline int GetMapSize() const { return m_size; }
};