#include "ShadowMap.h"
#include <iostream>
#include <gl_core_4_4.h>
#include <glm\mat4x4.hpp>


ShadowMap::ShadowMap(const int& a_size)
	: m_size(a_size)
{
	// Create shadow map buffer
	glGenFramebuffers(1, &m_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

	glGenTextures(1, &m_fboDepth);
	glBindTexture(GL_TEXTURE_2D, m_fboDepth);

	// Setup texture to use 16-bit depth componenet format
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, a_size, a_size, 
				 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// Attach depth texture, and only capture depth information
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, m_fboDepth, 0);

	// Disable colour capture
	glDrawBuffer(GL_NONE);

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "Framebuffer creation error..." << std::endl;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

ShadowMap::~ShadowMap()
{
	glDeleteBuffers(1, &m_fbo);
	glDeleteTextures(1, &m_fboDepth);
}

void ShadowMap::Bind()
{
	// Prepare rendering
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	glViewport(0, 0, m_size, m_size);
}

void ShadowMap::BindTexture(const int& a_unit)
{
	// Keep texture unit within bounds
#ifdef DEBUG
	assert(a_unit < 32);
#endif

	// Bind texture
	glActiveTexture(GL_TEXTURE0 + a_unit);
	glBindTexture(GL_TEXTURE_2D, m_fboDepth);
}