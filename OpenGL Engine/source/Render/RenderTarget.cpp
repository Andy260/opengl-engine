#include "RenderTarget.h"
#include <iostream>
#include <gl_core_4_4.h>
#ifdef DEBUG
#include <assert.h>
#endif


RenderTarget::RenderTarget(const int& a_width, const int& a_height)
{
	// Save render target size
	m_width = a_width;
	m_height = a_height;

	// Create framebuffer and bind
	glGenFramebuffers(1, &m_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

	// Create texture as render target
	glGenTextures(1, &m_fboTexture);
	glBindTexture(GL_TEXTURE_2D, m_fboTexture);

	// Specify format storage for texture
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, m_width, m_height);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Attach texture target to frame buffer as colour attachment
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_fboTexture, 0);

	// Create and bind a 24bit depth buffer as a render buffer
	glGenRenderbuffers(1, &m_depthBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, m_depthBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, m_width, m_height);

	// Attach depth buffer to frame buffer
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, 
							  GL_RENDERBUFFER, m_depthBuffer);

	// Assign colour attachments
	GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, drawBuffers);

	// Check for errors
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "Framebuffer creation error..." << std::endl;

	// Unbind
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

RenderTarget::~RenderTarget()
{
	glDeleteFramebuffers(1, &m_fbo);
	glDeleteTextures(1, &m_fboTexture);
	glDeleteRenderbuffers(1, &m_depthBuffer);
}

void RenderTarget::Bind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	glViewport(0, 0, m_width, m_height);
}

void RenderTarget::BindAsTexture(const int& a_unit)
{
	// Keep texture unit within bounds
#ifdef DEBUG
	assert(a_unit < 32);
#endif

	// Bind texture
	glActiveTexture(GL_TEXTURE0 + a_unit);
	glBindTexture(GL_TEXTURE_2D, m_fboTexture);
}

void RenderTarget::BindBackBuffer()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}