#include "Game.h"
#include <Time\GameTime.h>
#include <Input\Keyboard.h>
#include <Input\Mouse.h>
#include <GameWindow.h>
#include <TweakMenu.h>
#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>
#include <iostream>
#include <fstream>


Game::Game(const int &a_winWidth, const int &a_winHeight, 
		   const std::string &a_winName)
{
	// Initialise GLFW
	if (!GameWindow::Initialise())
		Terminate(true);

	m_pWindow = new GameWindow(a_winWidth, a_winHeight, a_winName, false);

	// Initialise OpenGL
	if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
	{
		Terminate(true);
	}

	// Print OpenGL version being used
	std::cout << "Renderer: OpenGL " << ogl_GetMajorVersion() << "." << ogl_GetMinorVersion() << std::endl;

	// Initialise input managers
	Keyboard::Intialise(m_pWindow);
	Mouse::Intialise(m_pWindow);
}

Game::~Game()
{
	delete m_pWindow;
	glfwTerminate();
}

void Game::Terminate(const bool &a_error)
{
	exit(a_error ? EXIT_FAILURE : EXIT_SUCCESS);
}

void Game::Run()
{
	Initialise();

	glfwSwapInterval(0);

	float deltaTime = 0.0f;
	while (!m_pWindow->ShouldClose())
	{
		GameTime::Update();
		deltaTime = GameTime::GetDeltaTime();

		Update(deltaTime);
		Draw(deltaTime);

		m_pWindow->SwapBuffers();
		glfwPollEvents();
	}

	Shutdown();
}