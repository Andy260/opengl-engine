#include "BoundingSphere.h"
#include <BoundingBox.h>
#include <BoundingFrustum.h>
#include <glm\glm.hpp>
#include <glm\ext.hpp>


BoundingSphere::BoundingSphere(const glm::vec3& a_centre,
	const float& a_radius) :
	m_centre(a_centre), m_radius(a_radius)
{

}

void BoundingSphere::Fit(const std::vector<glm::vec3>& a_pointsList)
{
	// Use similar method to fiting bounding box around
	// a list of points, but converting result into sphere data
	glm::vec3 min(1e37f), max(-1e37f);

	for (auto& point : a_pointsList) 
	{
		if (point.x < min.x) min.x = point.x;
		if (point.y < min.y) min.y = point.y;
		if (point.z < min.z) min.z = point.z;
		if (point.x > max.x) max.x = point.x;
		if (point.y > max.y) max.y = point.y;
		if (point.z > max.z) max.z = point.z;
	}

	m_centre = (min + max) * 0.5f;
	m_radius = glm::distance(min, m_centre);
}

void BoundingSphere::Fit(const std::vector<glm::vec3*>& a_pointsList)
{
	// Use similar method to fiting bounding box around
	// a list of points, but converting result into sphere data
	glm::vec3 min(1e37f), max(-1e37f);

	for (auto& point : a_pointsList)
	{
		if (point->x < min.x) min.x = point->x;
		if (point->y < min.y) min.y = point->y;
		if (point->z < min.z) min.z = point->z;
		if (point->x > max.x) max.x = point->x;
		if (point->y > max.y) max.y = point->y;
		if (point->z > max.z) max.z = point->z;
	}

	m_centre = (min + max) * 0.5f;
	m_radius = glm::distance(min, m_centre);
}

void BoundingSphere::Fit(const std::list<glm::vec3>& a_pointsList)
{
	// Use similar method to fiting bounding box around
	// a list of points, but converting result into sphere data
	glm::vec3 min(1e37f), max(-1e37f);

	for (auto& point : a_pointsList)
	{
		if (point.x < min.x) min.x = point.x;
		if (point.y < min.y) min.y = point.y;
		if (point.z < min.z) min.z = point.z;
		if (point.x > max.x) max.x = point.x;
		if (point.y > max.y) max.y = point.y;
		if (point.z > max.z) max.z = point.z;
	}

	m_centre = (min + max) * 0.5f;
	m_radius = glm::distance(min, m_centre);
}

void BoundingSphere::Fit(const std::list<glm::vec3*>& a_pointsList)
{
	// Use similar method to fiting bounding box around
	// a list of points, but converting result into sphere data
	glm::vec3 min(1e37f), max(-1e37f);

	for (auto& point : a_pointsList)
	{
		if (point->x < min.x) min.x = point->x;
		if (point->y < min.y) min.y = point->y;
		if (point->z < min.z) min.z = point->z;
		if (point->x > max.x) max.x = point->x;
		if (point->y > max.y) max.y = point->y;
		if (point->z > max.z) max.z = point->z;
	}

	m_centre = (min + max) * 0.5f;
	m_radius = glm::distance(min, m_centre);
}

bool BoundingSphere::Intersects(const BoundingBox& a_box)
{
	return false;
}

bool BoundingSphere::Intersects(const BoundingSphere& a_sphere)
{
	glm::vec3 relativePos = m_centre - a_sphere.m_centre;

	// RelativePos.x squared + RelativePos.y squared + 
	// RelativePos.z squared
	float distance = relativePos.x * relativePos.x + relativePos.y * 
		relativePos.y + relativePos.z * relativePos.z;

	float minDistance = m_radius + a_sphere.m_radius;

	return distance <= minDistance * minDistance;
}

bool BoundingSphere::Intersects(const BoundingFrustum& a_frustum)
{
	return false;
}