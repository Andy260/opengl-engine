#include "BoundingBox.h"
#include <BoundingFrustum.h>
#include <BoundingSphere.h>


BoundingBox::BoundingBox(const glm::vec3& a_min, const glm::vec3& a_max)
	: m_min(a_min), m_max(a_max)
{

}

void BoundingBox::Fit(const std::vector<glm::vec3>& a_pointsList)
{
	m_min.x = m_min.y = m_min.z = 1e37f;	// Max float
	m_max.x = m_max.y = m_max.z = -1e37f;	// Min float

	// Fit bouding box around list of points, 
	// resizing as needed
	for (auto& point : a_pointsList)
	{
		if (point.x < m_min.x) m_min.x = point.x;
		if (point.y < m_min.y) m_min.y = point.y;
		if (point.z < m_min.z) m_min.z = point.z;
		if (point.x > m_max.x) m_max.x = point.x;
		if (point.y > m_max.y) m_max.y = point.y;
		if (point.z > m_max.z) m_max.z = point.z;
	}
}

void BoundingBox::Fit(const std::vector<glm::vec3*>& a_pointsList)
{
	m_min.x = m_min.y = m_min.z = 1e37f;	// Max float
	m_max.x = m_max.y = m_max.z = -1e37f;	// Min float

	// Fit bouding box around list of points, 
	// resizing as needed
	for (auto point : a_pointsList)
	{
		if (point->x < m_min.x) m_min.x = point->x;
		if (point->y < m_min.y) m_min.y = point->y;
		if (point->z < m_min.z) m_min.z = point->z;
		if (point->x > m_max.x) m_max.x = point->x;
		if (point->y > m_max.y) m_max.y = point->y;
		if (point->z > m_max.z) m_max.z = point->z;
	}
}

void BoundingBox::Fit(const std::list<glm::vec3>& a_pointsList)
{
	m_min.x = m_min.y = m_min.z = 1e37f;	// Max float
	m_max.x = m_max.y = m_max.z = -1e37f;	// Min float

	// Fit bouding box around list of points, 
	// resizing as needed
	for (auto& point : a_pointsList)
	{
		if (point.x < m_min.x) m_min.x = point.x;
		if (point.y < m_min.y) m_min.y = point.y;
		if (point.z < m_min.z) m_min.z = point.z;
		if (point.x > m_max.x) m_max.x = point.x;
		if (point.y > m_max.y) m_max.y = point.y;
		if (point.z > m_max.z) m_max.z = point.z;
	}
}

void BoundingBox::Fit(const std::list<glm::vec3*>& a_pointsList)
{
	m_min.x = m_min.y = m_min.z = 1e37f;	// Max float
	m_max.x = m_max.y = m_max.z = -1e37f;	// Min float

	// Fit bouding box around list of points, 
	// resizing as needed
	for (auto point : a_pointsList)
	{
		if (point->x < m_min.x)	m_min.x = point->x;
		if (point->y < m_min.y) m_min.y = point->y;
		if (point->z < m_min.z) m_min.z = point->z;
		if (point->x > m_max.x)	m_max.x = point->x;
		if (point->y > m_max.y) m_max.y = point->y;
		if (point->z > m_max.z) m_max.z = point->z;
	}
}

bool BoundingBox::Intersects(const BoundingBox& a_box)
{
	return	m_max.x > a_box.m_min.x && m_min.x < a_box.m_max.x && 
			m_max.y > a_box.m_min.y && m_min.y < a_box.m_max.y &&
			m_max.z > a_box.m_min.z && m_min.z < a_box.m_max.z;
}

bool BoundingBox::Intersects(const BoundingSphere& a_sphere)
{
	return false;
}

bool BoundingBox::Intersects(const BoundingFrustum& a_frustum)
{
	return false;
}