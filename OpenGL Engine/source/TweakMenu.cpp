#include "TweakMenu.h"
#include <AntTweakBar.h>


TweakMenu::TweakMenu(std::string a_name)
	: m_name(a_name),
	  m_active(true)
{
	m_pTwBar = TwNewBar(a_name.c_str());
}

void TweakMenu::Intialise()
{
	TwInit(TW_OPENGL, NULL);
}

void TweakMenu::Deintialise()
{
	TwDeleteAllBars();
	TwTerminate();
}

void TweakMenu::DrawAllMenus()
{
	TwDraw();
}