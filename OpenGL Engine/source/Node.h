#pragma once
#include <list>
#include <glm\mat4x4.hpp>

class Node
{
protected:
	Node* m_pParent;
	std::list<Node*> m_children;

	glm::mat4 m_worldTransform;		// Relative to world origin
	glm::mat4 m_localTransform;		// Relative to parent origin

	bool m_dirtyWorld;				// Flags when world transform should be updated

	void UpdateTransforms();

public:
	Node();

	void SetParent(Node* a_node) { m_pParent = a_node; }
	void AddChild(Node* a_node);
	void RemoveChild(Node* a_node);

	void SetLocalTransform(const glm::mat4& a_transform) { m_localTransform = a_transform; }

	inline Node* GetParent() const { return m_pParent; }
	inline std::list<Node*> GetChildren() const { return m_children; }

	inline glm::mat4 GetLocalTransform() const { return m_localTransform; }
	glm::mat4 GetWorldTransform();
};