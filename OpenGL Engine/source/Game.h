#pragma once
#include <string>

class GameWindow;

class Game
{
protected:
	GameWindow* m_pWindow;

	// Shuts down the game
	void Terminate(const bool &a_error);

public:
	Game(const int &a_winWidth, const int &a_winHeight, 
		const std::string &a_winName);
	~Game();

	void virtual Initialise()						= 0;
	void virtual Update(const float &a_deltaTime)	= 0;
	void virtual Draw(const float &a_deltaTime)		= 0;
	void virtual Shutdown()							= 0;

	void Run();
};

