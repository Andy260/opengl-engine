#pragma once
#include <glm\fwd.hpp>
#include <Plane.h>

class BoundingSphere;
class BoundingBox;
class Plane;

class BoundingFrustum
{
private:
	Plane m_near, m_far, m_top, m_bottom, m_left, m_right;

public:
	BoundingFrustum(const glm::mat4& a_projectionView);

	bool Contains(const BoundingSphere& a_sphere);
	bool Contains(const BoundingBox& a_box);
	bool Contains(const Plane& a_plane);

	bool Intersects(const BoundingBox& a_box);
	bool Intersects(const BoundingSphere& a_sphere);
	bool Intersects(const BoundingFrustum& a_frustum);
	bool Intersects(const Plane& a_plane);

	inline Plane GetNear()		const { return m_near; }
	inline Plane GetFar()		const { return m_far; }
	inline Plane GetTop()		const { return m_top; }
	inline Plane GetBottom()	const { return m_bottom; }
	inline Plane GetLeft()		const { return m_left; }
	inline Plane GetRight()		const { return m_right; }
};