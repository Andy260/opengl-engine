#pragma once
#include <string>

struct GLFWwindow;
class Mouse;
class Keyboard;

class GameWindow
{
private:
	friend Mouse;
	friend Keyboard;

	// Handle to GLFW window
	GLFWwindow* m_pGLFWhandle;

	// If this window currently has focus (set by callback)
	bool m_hasFocus;

	static void ResizeCallBack(GLFWwindow* a_pWindow, int a_width, int a_height);

public:
	GameWindow(const int &a_width, const int &a_height, 
		const std::string &a_title, const bool &a_fullscreen);
	~GameWindow();

	static bool Initialise();

	int ShouldClose();

	void SwapBuffers();

	void SetSize(const int &a_width, const int &a_height);
	void SetTitle(const std::string &a_title);

	void GetFrameBufferSize(int& a_width, int& a_height);

	void GetSize(int* a_pWidth, int* a_pHeight);
};