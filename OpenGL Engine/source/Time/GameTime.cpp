#include "Time/GameTime.h"
#include <GLFW/glfw3.h>

float GameTime::prevTime = 0;
float GameTime::deltaTime = 0;

void GameTime::Update()
{
	float currTime = (float)glfwGetTime();
	deltaTime = currTime - prevTime;
	prevTime = currTime;
}

float GameTime::GetDeltaTime()
{
	return deltaTime;
}
