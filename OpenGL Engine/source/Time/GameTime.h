#pragma once


class GameTime
{
private:
	static float prevTime;
	static float deltaTime;

public:
	// Call once per Frame
	static void Update();

	// Gets current deltaTime since last update
	static float GetDeltaTime();
};

