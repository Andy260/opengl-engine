#include "GPUParticleEmitter.h"
#include <Camera\Camera.h>
#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>

GPUParticleEmitter::GPUParticleEmitter() 
:	m_particles(nullptr), m_maxParticles(0), 
	m_position(0.0f, 0.0f, 0.0f)
{
	m_vao[0] = 0;
	m_vao[1] = 0;
	m_vbo[0] = 0;
	m_vbo[1] = 0;
}

GPUParticleEmitter::~GPUParticleEmitter()
{
	delete[] m_particles;

	glDeleteVertexArrays(2, m_vao);
	glDeleteBuffers(2, m_vbo);
}

void GPUParticleEmitter::Initialise(const unsigned int& a_maxParticles,
	const float& a_lifespanMin, const float& a_lifespanMax,
	const float& a_velocityMin, const float& a_velocityMax,
	const float& a_startSize, const float& a_endSize,
	const glm::vec4& a_startColour,
	const glm::vec4& a_endColour)
{
	// Set emitter attributes
	m_startColour	= a_startColour;
	m_endColour		= a_endColour;
	m_startSize		= a_startSize;
	m_endSize		= a_endSize;
	m_velocityMin	= a_velocityMin;
	m_velocityMax	= a_velocityMax;
	m_lifespanMin	= a_lifespanMin;
	m_lifespanMax	= a_lifespanMax;
	m_maxParticles	= a_maxParticles;

	// Allocate particle data
	m_particles = new GPUParticle[a_maxParticles];

	// Set active buffer to first
	m_activeBuffer = 0;

	CreateBuffers();
	CreateShaders();
}

void GPUParticleEmitter::CreateBuffers()
{
	// create opengl buffers
	glGenVertexArrays(2, m_vao);
	glGenBuffers(2, m_vbo);

	// setup the first buffer
	glBindVertexArray(m_vao[0]);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, m_maxParticles *
		sizeof(GPUParticle), m_particles, GL_STREAM_DRAW);

	glEnableVertexAttribArray(0); // Position
	glEnableVertexAttribArray(1); // Velocity
	glEnableVertexAttribArray(2); // Lifetime
	glEnableVertexAttribArray(3); // Lifespan
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
		sizeof(GPUParticle), 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
		sizeof(GPUParticle), ((char*)0) + 12);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE,
		sizeof(GPUParticle), ((char*)0) + 24);
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE,
		sizeof(GPUParticle), ((char*)0) + 28);

	// setup the second buffer
	glBindVertexArray(m_vao[1]);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, m_maxParticles *
		sizeof(GPUParticle), 0, GL_STREAM_DRAW);

	glEnableVertexAttribArray(0); // Position
	glEnableVertexAttribArray(1); // Velocity
	glEnableVertexAttribArray(2); // Lifetime
	glEnableVertexAttribArray(3); // Lifespan
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
		sizeof(GPUParticle), 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
		sizeof(GPUParticle), ((char*)0) + 12);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE,
		sizeof(GPUParticle), ((char*)0) + 24);
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE,
		sizeof(GPUParticle), ((char*)0) + 28);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void GPUParticleEmitter::CreateShaders()
{
	//------------------------------------------------------------
	// Load update shader
	//------------------------------------------------------------
	m_updateShader.AddShaderFile("shaders/gpuParticleUpdate.vert",
		ShaderStage::VERTEX);

	// Set feedback variables
	const char* varyings[] = { "position", "velocity", 
								"lifetime", "lifespan" };
	m_updateShader.SetFeedbackVariables(4, varyings,
		ShaderBufferMode::INTERLEAVED);

	m_updateShader.LinkProgram();

	m_updateShader.Bind();

	// Set uniforms variables for update shader
	unsigned int uniform = m_updateShader.GetUniform("lifeMin");
	glUniform1f(uniform, m_lifespanMin);
	uniform = m_updateShader.GetUniform("lifeMax");
	glUniform1f(uniform, m_lifespanMax);
	//------------------------------------------------------------


	//------------------------------------------------------------
	// Load draw shader
	//------------------------------------------------------------
	m_drawShader.AddShaderFile("shaders/gpuParticle.vert",
		ShaderStage::VERTEX);
	m_drawShader.AddShaderFile("shaders/gpuParticle.geom",
		ShaderStage::GEOMETRY);
	m_drawShader.AddShaderFile("shaders/gpuParticle.frag",
		ShaderStage::FRAGMENT);

	m_drawShader.LinkProgram();

	m_drawShader.Bind();

	// Set size uniforms for draw shader
	uniform = m_drawShader.GetUniform("sizeStart");
	glUniform1f(uniform, m_startSize);
	uniform = m_drawShader.GetUniform("sizeEnd");
	glUniform1f(uniform, m_endSize);

	// Set colour uniforms for draw shader
	uniform = m_drawShader.GetUniform("colourStart");
	glUniform4fv(uniform, 1, &m_startColour[0]);
	uniform = m_drawShader.GetUniform("colourEnd");
	glUniform4fv(uniform, 1, &m_endColour[0]);
	//------------------------------------------------------------
}

void GPUParticleEmitter::Draw(const float& a_deltaTime, const Camera& a_camera)
{
	m_updateShader.Bind();

	// Send time information
	unsigned int uniform = m_updateShader.GetUniform("time");
	glUniform1f(uniform, (float)glfwGetTime());

	// Send Delta Time
	uniform = m_updateShader.GetUniform("deltaTime");
	glUniform1f(uniform, a_deltaTime);

	// Send emitter's position
	uniform = m_updateShader.GetUniform("emitterPosition");
	glUniform3fv(uniform, 1, &m_position[0]);

	// Disable rasterisation for feedback shader
	glEnable(GL_RASTERIZER_DISCARD);

	// Bind buffer to update
	glBindVertexArray(m_vao[m_activeBuffer]);

	// Work out the "other" buffer
	unsigned int otherBuffer = (m_activeBuffer + 1) % 2;

	// Bind the other buffer, and begin transform feedback
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 
					 m_vbo[otherBuffer]);
	glBeginTransformFeedback(GL_POINTS);

	glDrawArrays(GL_POINTS, 0, m_maxParticles);

	// Disable transform feedback and enable rasterisation again
	glEndTransformFeedback();
	glDisable(GL_RASTERIZER_DISCARD);
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);

	// Draw particles using Geometry shader to billboard them
	m_drawShader.Bind();

	// Set projectionView matrix uniform
	glm::mat4 unifromMat = a_camera.GetProjectionViewTransform();
	uniform = m_drawShader.GetUniform("projectionView");
	glUniformMatrix4fv(uniform, 1, GL_FALSE, &unifromMat[0][0]);

	// Set camera transform uniform
	unifromMat = a_camera.GetTransform();
	uniform = m_drawShader.GetUniform("cameraTransform");
	glUniformMatrix4fv(uniform, 1, GL_FALSE, &unifromMat[0][0]);

	// Draw particles in the "other" buffer
	glBindVertexArray(m_vao[otherBuffer]);
	glDrawArrays(GL_POINTS, 0, m_maxParticles);

	// Swap buffer for next frame
	m_activeBuffer = otherBuffer;
}