#pragma once
#include <glm\glm.hpp>
#include <Render\Shader.h>

class Camera;

class GPUParticleEmitter
{
protected:
	// Defines a particle
	struct GPUParticle
	{
		GPUParticle() : lifetime(1), lifespan(0) {}

		glm::vec3 position;
		glm::vec3 velocity;
		float lifetime;
		float lifespan;
	};

	GPUParticle* m_particles;

	// Max particles for this emitter
	unsigned int m_maxParticles;

	// Position of emitter
	glm::vec3 m_position;

	// Lifespan of particles
	float m_lifespanMin;
	float m_lifespanMax;

	// Velocity of particles
	float m_velocityMin;
	float m_velocityMax;

	// Size of particles
	float m_startSize;
	float m_endSize;

	// Colour of particles
	glm::vec4 m_startColour;
	glm::vec4 m_endColour;
	
	// OpenGL handles
	unsigned int m_activeBuffer;
	unsigned int m_vao[2];
	unsigned int m_vbo[2];

	Shader m_updateShader, m_drawShader;

	void CreateBuffers();
	void CreateShaders();

public:
	GPUParticleEmitter();
	~GPUParticleEmitter();

	void Initialise(const unsigned int& a_maxParticles,
					const float& a_lifespanMin, const float& a_lifespanMax,
					const float& a_velocityMin, const float& a_velocityMax,
					const float& a_startSize, const float& a_endSize,
					const glm::vec4& a_startColour,
					const glm::vec4& a_endColour);

	void Draw(const float& deltaTime, const Camera& a_camera);
};