#version 410

// Vertex inputs
in vec4 fPosition;
in vec4 fColour;
in vec2 fTexCoord;

// Final fragment colour
out vec4 fragColour;

uniform sampler2D diffuseTex;

void main()
{
	fragColour = texture(diffuseTex, fTexCoord) * fColour;
}