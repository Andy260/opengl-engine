#pragma once
#include <Game.h>

class Game1 : public Game
{
public:
	Game1();
	virtual ~Game1();

	virtual void Initialise() final;
	virtual void Update(const float& a_deltaTime) final;
	virtual void Draw(const float& a_deltaTime) final;
	virtual void Shutdown() final;
};

