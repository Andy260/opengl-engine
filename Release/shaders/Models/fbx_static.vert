#version 410

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normal;
layout(location = 2) in vec4 tangent;
layout(location = 3) in vec2 texCoord;
layout(location = 4) in vec4 weights;
layout(location = 5) in vec4 indices;

out vec3 fNormal;
out vec3 fPosition;
out vec3 fTangent;
out vec3 fBitangent;
out vec2 fTexCoord;

// Model View Projection Matrix
uniform mat4 mvp;

void main()
{
	// Fragment pass through
	fPosition	= position.xyz;
	fNormal 	= normal.xyz;
	fTangent 	= tangent.xyz;
	fBitangent 	= cross(normal.xyz, tangent.xyz);
	fTexCoord 	= texCoord;
	
	gl_Position = mvp * position;
}